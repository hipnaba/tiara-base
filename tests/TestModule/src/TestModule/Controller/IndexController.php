<?php
namespace TestModule\Controller;

use TiaraBase\Controller\Plugin\Wrapper;
use Zend\Mvc\Controller\AbstractActionController;

/**
 * Class IndexController
 * @package TestModule\Controller
 * @method Wrapper wrapper()
 */
class IndexController extends AbstractActionController
{ }