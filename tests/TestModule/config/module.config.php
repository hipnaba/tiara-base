<?php
return array(
    'service_manager' => array(
        'factories' => array(
            'Zend\Db\Adapter\Adapter' => 'Zend\Db\Adapter\AdapterServiceFactory'
        )
    ),
    'db' => array(
        'driver' => 'Pdo',
        'dsn' => 'mysql:dbname=test;host=localhost',
        'driver_options' => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'Test\Controller\Index' => 'TestModule\Controller\IndexController'
        )
    ),
    'entities' => array(
        'invokables' => array(
            'SomeEntity' => 'TiaraBaseTest\Entity\TestAsset\SomeEntity',
            'Album' => 'TiaraBaseTest\Entity\TestAsset\Album',
            'AlbumCollection' => 'TiaraBaseTest\Entity\TestAsset\AlbumCollection',
            'AnEntity' => 'TiaraBaseTest\Entity\TestAsset\AnEntity',
            'NotAnEntity' => 'TiaraBaseTest\Entity\TestAsset\NotAnEntity',
        )
    ),
    'mappers' => array(
        'invokables' => array(
            'AnotherEventMapper' => 'TiaraBaseTest\Mapper\TestAsset\AnotherEventMapper',
            'EventMapper' => 'TiaraBaseTest\Mapper\TestAsset\EventMapper',
            'AlbumMapper' => 'TiaraBaseTest\Mapper\TestAsset\AlbumMapper',
            'AMapper' => 'TiaraBaseTest\Mapper\TestAsset\AMapper',
            'NotAMapper' => 'TiaraBaseTest\Mapper\TestAsset\NotAMapper'
        )
    ),
    'view_manager' => array(
        'display_not_found_reason' => false,
        'display_exceptions' => false,
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    'router' => array(
        'routes' => array(
            'index' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/',
                    'defaults' => array(
                        'controller' => 'Test\Controller\Index',
                        'action' => 'index'
                    )
                )
            )
        )
    )
);