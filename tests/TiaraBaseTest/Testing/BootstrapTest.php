<?php
namespace TiaraBaseTest\Testing;

use TiaraBase\Testing\Bootstrap;

class BootstrapTest extends \PHPUnit_Framework_TestCase
{
    public function testCanReadConfiguration()
    {
        $configuration = Bootstrap::getApplicationConfiguration();

        $base = dirname(dirname(dirname(__DIR__)));
        $expectedPaths = array(
            $base . '/tests',
            $base . '/vendor',
            $base
        );

        $this->assertNotEmpty($configuration);
        $this->assertTrue(isset($configuration['modules']));
        $this->assertEquals(array('TiaraBase', 'TestModule'), $configuration['modules']);
        $this->assertTrue(isset($configuration['module_listener_options']));
        $this->assertTrue(isset($configuration['module_listener_options']['module_paths']));
        $this->assertEquals($expectedPaths, array_values($configuration['module_listener_options']['module_paths']));
    }

    public function testApplicationIsShared()
    {
        $application = Bootstrap::getApplication();
        $this->assertSame($application, Bootstrap::getApplication());
    }

    public function testApplicationIsNotShared()
    {
        $application = Bootstrap::getApplication(true);
        $this->assertNotSame($application, Bootstrap::getApplication());
    }

    public function testServiceManagerIsShared()
    {
        $application = Bootstrap::getApplication();
        $services = $application->getServiceManager();
        $this->assertSame($services, Bootstrap::getServiceManager());
    }

    public function testServiceManagerIsNotShare()
    {
        $application = Bootstrap::getApplication();
        $services = $application->getServiceManager();
        $this->assertNotSame($services, Bootstrap::getServiceManager(true));
    }
}