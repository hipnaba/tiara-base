<?php
namespace TiaraBaseTest\Entity\TestAsset;

use TiaraBase\Entity\AbstractEntity;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerAwareTrait;

class Album extends AbstractEntity implements EventManagerAwareInterface
{
    use EventManagerAwareTrait;

    protected $artist;
    protected $title;

    /**
     * @return mixed
     */
    public function getArtist()
    {
        return $this->artist;
    }

    /**
     * @param mixed $artist
     */
    public function setArtist($artist)
    {
        $this->artist = $artist;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getCalled()
    {
        $called = null;
        $this->getEventManager()->trigger('Album.callTarget', function ($value) use(&$called) {
            $called = $value;
        });
        return $called;
    }
}