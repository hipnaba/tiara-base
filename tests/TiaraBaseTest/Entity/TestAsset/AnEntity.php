<?php
namespace TiaraBaseTest\Entity\TestAsset;

use TiaraBase\Entity\AbstractEntity;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class AnEntity extends AbstractEntity implements
    ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;
}