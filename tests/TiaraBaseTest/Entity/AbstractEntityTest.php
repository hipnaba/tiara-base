<?php
namespace TiaraBaseTest\Entity;

use TiaraBase\Testing\Bootstrap;

class AbstractEntityTest extends \PHPUnit_Framework_TestCase
{
    /** @var \TiaraBase\Entity\EntityManager */
    protected $entities;
    /** @var \Zend\ServiceManager\ServiceManager */
    protected $services;

    protected function setUp()
    {
        $this->services = Bootstrap::getServiceManager();
        $this->entities = $this->services->get('EntityManager');
    }

    public function testGettersAndSettersWork()
    {
        /** @var \TiaraBaseTest\Entity\TestAsset\AnEntity $entity */
        $entity = $this->entities->get('AnEntity');

        $entity->setId(12);
        $this->assertEquals(12, $entity->getId());
    }

    /**
     * @expectedException \TiaraBase\Entity\Exception\DomainException
     */
    public function testEntityThrowsExceptionWhenTryingToChangeTheId()
    {
        $entity = $this->entities->get('AnEntity');
        $entity->setId(12);
        $entity->setId(13);
    }

    /**
     * @expectedException \TiaraBase\Entity\Exception\InvalidArgumentException
     */
    public function testEntityThrowsExceptionWhenTryingToSetTheIdToANonNumericValue()
    {
        $entity = $this->entities->get('AnEntity');
        $entity->setId('string');
    }
}