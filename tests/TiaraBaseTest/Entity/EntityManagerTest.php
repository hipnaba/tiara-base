<?php
namespace TiaraBaseTest\Entity;

use TiaraBase\Testing\Bootstrap;

class EntityManagerTest extends \PHPUnit_Framework_TestCase
{
    /** @var \Zend\ServiceManager\ServiceManager */
    protected $services;

    protected function setUp()
    {
        $this->services = Bootstrap::getServiceManager(true);
    }

    /**
     * @return \TiaraBase\Entity\EntityManager
     */
    protected function getEntityManager()
    {
        return $this->services->get('EntityManager');
    }

    public function testServiceManagerCanCreateTheEntityManager()
    {
        $entities = $this->getEntityManager();

        $this->assertInstanceOf('TiaraBase\Entity\EntityManager', $entities);
        $this->assertSame($entities, $this->services->get('EntityManager'));
        $this->assertSame($this->services, $entities->getServiceLocator());
    }

    public function testCanCreateNewEntitiesFromMergedConfig()
    {
        $entities = $this->getEntityManager();
        $entity = $entities->get('AnEntity');
        $collection = $entities->get('EntityCollection');

        $this->assertInstanceOf('TiaraBase\Entity\EntityInterface', $entity);
        $this->assertNotSame($entity, $entities->get('AnEntity'));
        $this->assertInstanceOf('TiaraBase\Entity\EntityCollectionInterface', $collection);
        $this->assertNotSame($collection, $entities->get('EntityCollection'));
    }

    public function testCanCreateEntityFromModuleConfig()
    {
        $entity = $this->getEntityManager()->get('ModuleEntity');

        $this->assertInstanceOf('TiaraBase\Entity\EntityInterface', $entity);
    }

    /**
     * @expectedException \TiaraBase\Entity\Exception\InvalidEntityException
     */
    public function testThrowsExceptionWhenTryingToCreateANonEntity()
    {
        $this->getEntityManager()->get('NotAnEntity');
    }

    public function testInjectsTheCorrectServiceManager()
    {
        $entities = $this->getEntityManager();
        /** @var \Zend\ServiceManager\ServiceLocatorAwareInterface $entity */
        $entity = $entities->get('AnEntity');

        $this->assertSame($entity->getServiceLocator(), $this->getEntityManager());
    }
}