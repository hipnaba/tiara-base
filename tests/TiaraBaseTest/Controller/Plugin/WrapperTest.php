<?php
namespace TiaraBaseTest\Controller\Plugin;

use TiaraBase\Testing\Bootstrap;

class WrapperTest extends \PHPUnit_Framework_TestCase
{
    /** @var \TiaraBase\Controller\Plugin\Wrapper */
    protected $plugin;

    public function setUp()
    {
        $services = Bootstrap::getServiceManager();
        $plugins = $services->get('ControllerPluginManager');
        $this->plugin = $plugins->get('Wrapper');
    }

    protected function tearDown()
    {
        $this->plugin->setWrapperTemplate(null);
    }

    public function testIsRegistered()
    {
        $this->assertInstanceOf('TiaraBase\Controller\Plugin\Wrapper', $this->plugin);
    }

    public function testIsCallable()
    {
        $plugin = $this->plugin;

        $plugin('wrapper-template');
        $this->assertEquals('wrapper-template', $plugin->getWrapperTemplate());
    }

    public function testWrapperWorks()
    {
        $this->plugin->setWrapperTemplate('wrapper');

        $response = Bootstrap::getResponse();
        $content = preg_replace('/\s/', ' ', $response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('layout wrapper index', $content);
    }
}