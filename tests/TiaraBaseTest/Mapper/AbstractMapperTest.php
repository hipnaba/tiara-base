<?php
namespace TiaraBaseTest\Mapper;

use PHPUnit_Extensions_Database_DataSet_IDataSet;
use PHPUnit_Extensions_Database_DB_IDatabaseConnection;
use TiaraBase\Testing\Bootstrap;

class AbstractMapperTest extends \PHPUnit_Extensions_Database_TestCase
{
    /** @var \Zend\ServiceManager\ServiceManager */
    protected $services;
    protected $conn;

    /**
     * Returns the test database connection.
     *
     * @return PHPUnit_Extensions_Database_DB_IDatabaseConnection
     */
    protected function getConnection()
    {
        if (null === $this->conn) {
            $pdo = new \PDO('mysql:dbname=test;host=localhost');
            $this->conn = $this->createDefaultDBConnection($pdo);
        }
        return $this->conn;
    }

    /**
     * Returns the test dataset.
     *
     * @return PHPUnit_Extensions_Database_DataSet_IDataSet
     */
    protected function getDataSet()
    {
        return $this->createFlatXMLDataSet(__DIR__ . '/../../TestModule/db/album.xml');
    }

    protected function setUp()
    {
        parent::setUp();

        $this->services = Bootstrap::getServiceManager();
    }

    /**
     * @return \TiaraBase\Mapper\MapperManager
     */
    protected function getMapperManager()
    {
        return $this->services->get('MapperManager');
    }

    public function testHasDefaultHydrator()
    {
        $mapper = $this->getMapperManager()->get('AMapper');

        $this->assertInstanceOf('Zend\StdLib\Hydrator\HydratorInterface', $mapper->getHydrator());
    }

    public function testCanCount()
    {
        /** @var \TiaraBaseTest\Mapper\TestAsset\AlbumMapper $mapper */
        $mapper = $this->getMapperManager()->get('AlbumMapper');

        $count = count($mapper);
        $this->assertEquals(5, $count);

        $count = $mapper->countByArtist('Gotye');
        $this->assertEquals(1, $count);
    }

    public function testFetchAll()
    {
        $mapper = $this->getMapperManager()->get('AlbumMapper');
        $albums = $mapper->fetchAll();

        $this->assertInstanceOf('TiaraBase\Entity\EntityCollectionInterface', $albums);
        $this->assertEquals(5, count($albums));
        foreach ($albums as $album) {
            /** @var \TiaraBaseTest\Entity\TestAsset\Album $album */
            $this->assertInstanceOf('TiaraBaseTest\Entity\TestAsset\Album', $album);
            $this->assertNotNull($album->getId());
        }
    }

    public function testEventsGetTriggeredAndResultsPopulated()
    {
        $mapper = $this->getMapperManager()->get('AnotherEventMapper');
        $result = $mapper->getEventManager()->trigger('some-event');

        $this->assertTrue($result->contains('another event triggered'));
    }

    public function testFindOneBy()
    {
        /** @var \TiaraBaseTest\Mapper\TestAsset\AlbumMapper $mapper */
        $mapper = $this->getMapperManager()->get('AlbumMapper');
        $album = $mapper->findByArtist('Gotye');

        $this->assertNotNull($album);
        $this->assertInstanceOf('TiaraBaseTest\Entity\TestAsset\Album', $album);
        $this->assertEquals('Making Mirrors', $album->getTitle());
    }

    public function testFindOneByViaEvent()
    {
        $mapper = $this->getMapperManager()->get('AlbumMapper');
        $album = $mapper->getEventManager()->trigger('findByArtist', $this, array('Gotye'))->first();

        $this->assertNotNull($album);
        $this->assertInstanceOf('TiaraBaseTest\Entity\TestAsset\Album', $album);
        $this->assertEquals('Making Mirrors', $album->getTitle());
    }

    public function testEventHandlerWillCallTargetIfCallable()
    {
        /** @var \TiaraBaseTest\Mapper\TestAsset\AlbumMapper $mapper */
        $mapper = $this->getMapperManager()->get('AlbumMapper');
        $album = $mapper->findByArtist('Gotye');

        $this->assertEquals('called', $album->getCalled());
    }

    public function testCanSaveNewEntities()
    {
        /** @var \TiaraBaseTest\Mapper\TestAsset\AlbumMapper $mapper */
        $mapper = $this->getMapperManager()->get('AlbumMapper');
        /** @var \TiaraBaseTest\Entity\TestAsset\Album $album */
        $album = $mapper->getEntityPrototype();

        $album->setArtist('Hladno Pivo');
        $album->setTitle('Desetka');
        $mapper->save($album);

        $this->assertEquals(6, $album->getId());
    }

    public function testCanDeleteEntities()
    {
        /** @var \TiaraBaseTest\Mapper\TestAsset\AlbumMapper $mapper */
        $mapper = $this->getMapperManager()->get('AlbumMapper');
        /** @var \TiaraBaseTest\Entity\TestAsset\Album $album */
        $album = $mapper->findByArtist('Gotye');
        $mapper->delete($album);

        $this->assertEquals(4, count($mapper));
    }
}