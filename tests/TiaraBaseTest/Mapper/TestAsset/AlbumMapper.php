<?php
namespace TiaraBaseTest\Mapper\TestAsset;

use TiaraBase\Mapper\AbstractMapper;

class AlbumMapper extends AbstractMapper
{
    protected $entityPrototypeName = 'Album';
    protected $table = 'album';

    /**
     * @param string $artistName
     * @return int
     */
    public function countByArtist($artistName)
    {
        return $this->countBy(array(
            'artist' => $artistName
        ));
    }

    public function callTarget()
    {
        return 'called';
    }

    /**
     * @param $artist
     * @return null|\TiaraBaseTest\Entity\TestAsset\Album
     */
    public function findByArtist($artist)
    {
        return $this->findOneBy('artist', $artist);
    }
}