<?php
namespace TiaraBaseTest\Mapper\TestAsset;

use TiaraBase\Mapper\AbstractMapper;
use TiaraBase\Mapper\MapperInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\Stdlib\InitializableInterface;

class AMapper extends AbstractMapper implements
    InitializableInterface,
    MapperInterface,
    ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    protected $entityPrototypeName = 'AnEntity';

    public $initCalled = false;

    /**
     * Init an object
     *
     * @return void
     */
    public function init()
    {
        $this->initCalled = true;


    }
}