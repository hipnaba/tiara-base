<?php
namespace TiaraBaseTest\Mapper\TestAsset;

use TiaraBase\Mapper\AbstractMapper;

class AnotherEventMapper extends AbstractMapper
{
    protected $entityPrototypeName = 'AnEntity';

    public function event()
    {
        return 'another event triggered';
    }

    public function init()
    {
        $this->events->attach('some-event', array($this, 'event'));
    }
}