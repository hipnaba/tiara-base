<?php
namespace TiaraBaseTest\Mapper\TestAsset;

use TiaraBase\Mapper\AbstractMapper;

class EventMapper extends AbstractMapper
{
    protected $entityPrototypeName = 'SomeEntity';

    public function event()
    {
        return 'event triggered';
    }

    public function init()
    {
        $this->events->attach('some-event', array($this, 'event'));
    }
}