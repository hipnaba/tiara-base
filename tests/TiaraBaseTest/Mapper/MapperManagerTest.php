<?php
namespace TiaraBaseTest\Mapper;

use TiaraBase\Testing\Bootstrap;

class MapperManagerTest extends \PHPUnit_Framework_TestCase
{
    /** @var \Zend\ServiceManager\ServiceManager */
    protected $services;

    protected function setUp()
    {
        $this->services = Bootstrap::getServiceManager(true);
    }

    /**
     * @return \TiaraBase\Mapper\MapperManager
     */
    protected function getMapperManager()
    {
        return $this->services->get('MapperManager');
    }

    public function testServiceManagerCanCreateTheMapperManager()
    {
        $mappers = $this->getMapperManager();

        $this->assertInstanceOf('TiaraBase\Mapper\MapperManager', $mappers);
        $this->assertSame($mappers, $this->services->get('MapperManager'));
        $this->assertSame($this->services, $mappers->getServiceLocator());
    }

    public function testCanCreateMapperFromMergedConfig()
    {
        $mapper = $this->getMapperManager()->get('AMapper');

        $this->assertInstanceOf('TiaraBase\Mapper\MapperInterface', $mapper);
        $this->assertSame($mapper, $this->getMapperManager()->get('AMapper'));
    }

    public function _testCanCreateMapperFromModuleConfig()
    {
        $mapper = $this->getMapperManager()->get('ModuleMapper');

        $this->assertInstanceOf('TiaraBase\Mapper\MapperInterface', $mapper);
    }

    /**
     * @expectedException \TiaraBase\Mapper\Exception\InvalidMapperException
     */
    public function testThrowsExceptionWhenTryingToCreateANonMapper()
    {
        $this->getMapperManager()->get('NotAMapper');
    }

    public function testInjectsTheCorrectServiceManager()
    {
        $mappers = $this->getMapperManager();
        /** @var \Zend\ServiceManager\ServiceLocatorAwareInterface $mapper */
        $mapper = $mappers->get('AMapper');

        $this->assertSame($mapper->getServiceLocator(), $mappers);
    }

    public function testCallsInitOnNewMappers()
    {
        /** @var \TiaraBaseTest\Mapper\TestAsset\AMapper $mapper */
        $mapper = $this->getMapperManager()->get('AMapper');

        $this->assertTrue($mapper->initCalled);
    }

    public function testInitializesThePrototype()
    {
        $mapper = $this->getMapperManager()->get('AMapper');

        $this->assertInstanceOf('TiaraBaseTest\Entity\TestAsset\AnEntity', $mapper->getEntityPrototype());
    }

    public function testCanForwardEventToNewMapper()
    {
        $mappers = $this->getMapperManager();
        $result = $mappers->getEventManager()->trigger('SomeEntity.some-event');

        $this->assertTrue($result->contains('event triggered'));
    }
}