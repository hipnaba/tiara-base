<?php
namespace TiaraBase\Controller\Plugin;

use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventsCapableInterface;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\Mvc\MvcEvent;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\Stdlib\CallbackHandler;
use Zend\View\Model\ViewModel;

/**
 * View script wrapper
 *
 * This plugin enables you to wrap another view script around the ViewModel produced
 * by the controller. Basically, an implementation of sub-layouts.
 *
 * @package TiaraBase\Controller\Plugin
 */
class Wrapper extends AbstractPlugin implements
    EventsCapableInterface,
    ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /** @var string */
    protected $wrapperTemplate;
    /** @var CallbackHandler */
    protected $listener;
    /** @var EventManagerInterface */
    protected $events;

    /**
     * This plugin is invokable
     *
     * @param null|string $wrapperTemplate
     * @return $this
     */
    public function __invoke($wrapperTemplate = null)
    {
        $this->setWrapperTemplate($wrapperTemplate);
        return $this;
    }

    /**
     * Returns the template used by the wrapper if any
     *
     * @return null|string
     */
    public function getWrapperTemplate()
    {
        return $this->wrapperTemplate ?: null;
    }

    /**
     * Sets the template to be used by the wrapper
     *
     * If the template is empty the wrapper is removed
     *
     * @param string $wrapperTemplate
     * @return $this
     */
    public function setWrapperTemplate($wrapperTemplate = null)
    {
        $this->wrapperTemplate = (string) $wrapperTemplate;

        if (strlen($this->wrapperTemplate) > 0) {
            $this->listener = $this->getEventManager()->attach(MvcEvent::EVENT_RENDER, array($this, 'onRender'));
        }
        else if (null !== $this->listener) {
            $this->getEventManager()->detach($this->listener);
        }

        return $this;
    }

    /**
     * Retrieve the event manager
     *
     * Lazy-loads an EventManager instance if none registered.
     *
     * @return EventManagerInterface
     */
    public function getEventManager()
    {
        if (null === $this->events) {
            /** @var \Zend\Mvc\Controller\PluginManager $plugins */
            $plugins = $this->getServiceLocator();
            $services = $plugins->getServiceLocator();
            /** @var \Zend\Mvc\ApplicationInterface $application */
            $application = $services->get('Application');

            $this->events = $application->getEventManager();
        }

        return $this->events;
    }

    /**
     * Wraps the response if needed
     *
     * @param MvcEvent $event
     */
    public function onRender(MvcEvent $event)
    {
        if (strlen($this->wrapperTemplate) > 0) {
            $result = $event->getResult();
            if ($result instanceof ViewModel) {
                $view = new ViewModel($result->getVariables());
                $view->setTemplate($result->getTemplate());

                $result->clearVariables()
                       ->clearChildren()
                       ->addChild($view)
                       ->setTemplate($this->wrapperTemplate);
            }
        }
    }
}