<?php
namespace TiaraBase\Testing;

use Zend\Console\Console;
use Zend\Mvc\Application;
use Zend\Stdlib\ArrayUtils;

error_reporting(E_ALL | E_STRICT);

/**
 * Provides acces to different components of a configured ZF2 application
 *
 * @package TiaraBase\Testing
 */
class Bootstrap
{
    /** @var Application */
    protected static $application;
    /** @var array */
    protected static $applicationConfiguration;
    /** @var string */
    protected static $path = __DIR__;

    /**
     * Returns the array used to configure the application
     *
     * If an application.config.php file exists in the same dir as the phpunit.xml used
     * to run the tests, it will be merged with some basic configuration
     *
     * @return array
     */
    public static function getApplicationConfiguration()
    {
        if (null === static::$applicationConfiguration) {
            self::$applicationConfiguration = array();

            if (false !== ($i = array_search('--configuration', $_SERVER['argv']))) {
                static::$path = dirname($_SERVER['argv'][$i+1]);
                chdir(static::$path);

                $vendor = static::findParentPath('vendor');
                /** @var \Composer\Autoload\ClassLoader $loader */
                $loader = include $vendor . '/autoload.php';

                foreach (scandir(static::$path) as $dir) {
                    if (!preg_match('/^\.+$/', $dir) && is_dir(static::$path . '/' . $dir)) {
                        $loader->setPsr4($dir . '\\', static::$path . '/' . $dir);
                    }
                }

                // use ModuleManager to load this module and it's dependencies
                $configuration = array(
                    'module_listener_options' => array(
                        'module_paths' => array(
                            self::$path,
                            $vendor,
                            basename(dirname(self::$path)) => dirname(self::$path)
                        )
                    ),
                );

                // Load the user-defined test configuration file, if it exists;
                if (file_exists(self::$path . '/application.config.php')) {
                    $testConfiguration = include self::$path . '/application.config.php';
                    $configuration = ArrayUtils::merge($configuration, $testConfiguration);
                }

                static::$applicationConfiguration = $configuration;
            }
        }
        return static::$applicationConfiguration;
    }

    /**
     * Returns a configured ZF2 application
     *
     * @param bool $new if true the application will be new, otherwise a shared instance is returned
     * @return Application
     */
    public static function getApplication($new = false)
    {
        if (null === static::$application || $new == true) {
            Console::overrideIsConsole(false);
            $configuration = self::getApplicationConfiguration();
            $application = Application::init($configuration);

            $services = $application->getServiceManager();
            $events = $application->getEventManager();
            /** @var \Zend\Mvc\SendResponseListener $listener */
            $listener = $services->get('SendResponseListener');
            $events->detach($listener);

            return $new ? $application : (static::$application = $application);
        }
        return self::$application;
    }

    /**
     * Returns the service manager used by the application
     *
     * @param bool $new
     * @return \Zend\ServiceManager\ServiceManager
     */
    public static function getServiceManager($new = false)
    {
        return static::getApplication($new)->getServiceManager();
    }

    /**
     * @return \Zend\Http\Response
     */
    public static function getResponse()
    {
        $application = self::getApplication();
        $application->run();

        return $application->getResponse();
    }

    /**
     * @param string $path
     * @return bool|string
     */
    protected static function findParentPath($path)
    {
        $dir = static::$path;
        $previousDir = '.';
        while (!is_dir($dir . '/' . $path)) {
            $dir = dirname($dir);
            if ($previousDir === $dir) return false;
            $previousDir = $dir;
        }
        return $dir . '/' . $path;
    }
}