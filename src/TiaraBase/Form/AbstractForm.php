<?php
namespace TiaraBase\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class AbstractForm extends Form implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /**
     * Retrieve input filter used by this form
     *
     * @throws Exception\InvalidInputFilterException
     * @return null|InputFilter
     */
    public function getInputFilter()
    {
        if (is_string($this->filter)) {
            /** @var \Zend\Form\FormElementManager $forms */
            $forms = $this->getServiceLocator();
            /** @var \Zend\ServiceManager\ServiceManager $services */
            $services = $forms->getServiceLocator();
            /** @var |Zend\InputFilter\InputFilterPluginManager $filters */
            $filters = $services->get('InputFilterManager');

            if ($filters->has($this->filter)) {
                /** @var InputFilter $filter */
                $filter = $filters->get($this->filter);

                foreach ($filter->getInputs() as $name => $input) {
                    if (!$this->has($name)) {
                        $filter->remove($name);
                    }
                }

                $this->filter = $filter;
            }
            else {
                throw new Exception\InvalidInputFilterException(sprintf(
                    "No input filter registered under the key '%s'",
                    $this->filter
                ));
            }
        }
        return parent::getInputFilter();
    }
}