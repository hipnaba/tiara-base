<?php
namespace TiaraBase\Form\Exception;

/**
 * Exception thrown if an argument does not match with the expected value.
 *
 * @package TiaraBase\Form\Exception
 */
class InvalidArgumentException extends \InvalidArgumentException implements
    ExceptionInterface
{ }