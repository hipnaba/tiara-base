<?php
namespace TiaraBase\Form\Exception;

/**
 * Interface implemented by all exceptions in the TiaraBase\Form module.
 *
 * @package TiaraBase\Form\Exception
 */
interface ExceptionInterface
{ }