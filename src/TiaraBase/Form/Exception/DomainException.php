<?php
namespace TiaraBase\Form\Exception;

/**
 * Exception thrown if a value does not adhere to a defined valid data domain.
 *
 * @package TiaraBase\Form\Exception
 */
class DomainException extends \DomainException implements
    ExceptionInterface
{ }