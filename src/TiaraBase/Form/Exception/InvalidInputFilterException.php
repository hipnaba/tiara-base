<?php
namespace TiaraBase\Form\Exception;

/**
 * Exception thrown if an argument isn't a valid input filter.
 *
 * @package TiaraBase\Form\Exception
 */
class InvalidInputFilterException extends InvalidArgumentException
{ }