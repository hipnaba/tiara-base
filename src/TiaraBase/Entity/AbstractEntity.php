<?php
namespace TiaraBase\Entity;

/**
 * Abstract entity.
 *
 * A base entity class. Provides the basic entity implementation. It is recommended
 * to extend this class instead of implementing EntityInterface since it provides
 * default implementations for interface methods.
 *
 * @package TiaraBase\Entity
 */
abstract class AbstractEntity implements
    EntityInterface
{
    /** @var int */
    protected $id;

    /**
     * Returns the entity ID
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the entity ID
     *
     * @param int $id
     * @throws Exception\DomainException when trying to set the ID of an entity that already
     *                                   has one set
     * @throws Exception\InvalidArgumentException when the supplied ID isn't a numerical value
     * @return $this
     */
    public function setId($id)
    {
        if (isset($this->id)) {
            throw new Exception\DomainException(
                'Trying to change the identity of an existing entity'
            );
        }

        if (!is_numeric($id)) {
            throw new Exception\InvalidArgumentException(sprintf(
                'The supplied ID must be a numerical value, %s sent instead',
                gettype($id)
            ));
        }

        $this->id = (int) $id;
        return $this;
    }
}