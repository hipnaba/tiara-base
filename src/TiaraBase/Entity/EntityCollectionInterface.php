<?php
namespace TiaraBase\Entity;

/**
 * Represents a collection of entities.
 *
 * Entities collections are lazy-loaded by an initializer callable. The initializer is usually
 * a closure provided by the mapper used to map the entity type in this collection. Although
 * implementing this interface directly will work, it is recommended to extend the AbstractEntityCollection
 * class found in this package. If no special logic is needed for a specific collection, a default implementation
 * is provided in the EntityCollection class.
 *
 * @package TiaraBase\Entity
 */
interface EntityCollectionInterface extends
    \Countable,
    \Iterator
{
    /**
     * Sets the initializer used to populate this collection. The initializer should accept
     * one argument, this collection.
     *
     * @param callable $initializer
     * @return $this
     */
    public function setInitializer($initializer);

    /**
     * Adds an entity to the collection
     *
     * @param EntityInterface $entity
     * @return $this
     */
    public function add(EntityInterface $entity);

    /**
     * Adds the entities from another collection to this collection
     *
     * @param EntityCollectionInterface $collection
     * @return $this
     */
    public function addAll(EntityCollectionInterface $collection);
}