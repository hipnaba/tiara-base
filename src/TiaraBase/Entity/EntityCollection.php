<?php
namespace TiaraBase\Entity;

/**
 * Default entity collection
 *
 * @package TiaraBase\Entity
 */
class EntityCollection extends AbstractEntityCollection
{ }