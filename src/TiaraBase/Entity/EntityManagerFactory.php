<?php
namespace TiaraBase\Entity;

use Zend\Mvc\Service\AbstractPluginManagerFactory;

/**
 * Creates a new entity manager.
 *
 * @package TiaraBase\Entity
 */
class EntityManagerFactory extends AbstractPluginManagerFactory
{
    const PLUGIN_MANAGER_CLASS = 'TiaraBase\Entity\EntityManager';
}