<?php
namespace TiaraBase\Entity;

/**
 * Indicates this module provides entities.
 *
 * @package TiaraBase\Entity
 */
interface EntityProviderInterface
{
    /**
     * Expected to return \Zend\ServiceManager\Config object or array to
     * seed such an object.
     *
     * @return array|\Zend\ServiceManager\Config
     */
    public function getEntityConfig();
}