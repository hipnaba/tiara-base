<?php
namespace TiaraBase\Entity;

use TiaraBase\Entity\Exception\InvalidArgumentException;

/**
 * Entity collection, supports lazy loading.
 *
 * @package TiaraBase\Entity
 */
abstract class AbstractEntityCollection implements
    EntityCollectionInterface
{

    /** @var EntityInterface[] */
    protected $entities = array();
    /** @var callable */
    protected $initializer;
    /** @var bool */
    protected $init = false;
    /** @var int */
    protected $position = 0;

    /**
     * Triggers the loading
     */
    protected function init()
    {
        if (is_callable($this->initializer)) {
            call_user_func($this->initializer, $this);
        }
        $this->init = true;
    }

    /**
     * Sets the initializer used to populate this collection. The initializer should accept
     * one argument, this collection
     *
     * @param callable $initializer
     * @throws Exception\InvalidArgumentException
     * @return $this
     */
    public function setInitializer($initializer)
    {
        if (!is_callable($initializer)) {
            throw new InvalidArgumentException(sprintf(
                'EntityCollection needs a callable set as an initializer, %s sent instead',
                gettype($initializer)
            ));
        }
        $this->initializer = $initializer;
        return $this;
    }

    /**
     * Adds an entity to the collections
     *
     * @param EntityInterface $entity
     * @return $this
     */
    public function add(EntityInterface $entity)
    {
        $this->entities[] = $entity;
        return $this;
    }

    /**
     * Adds the entities from another collection to this collection
     *
     * @param EntityCollectionInterface $collection
     * @return $this
     */
    public function addAll(EntityCollectionInterface $collection)
    {
        foreach ($collection as $entity) {
            $this->add($entity);
        }
        return $this;
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Count elements of an object
     * @link http://php.net/manual/en/countable.count.php
     * @return int The custom count as an integer.
     * </p>
     * <p>
     * The return value is cast to an integer.
     */
    public function count()
    {
        if (!$this->init) $this->init();
        return count($this->entities);
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Rewind the Iterator to the first element
     * @link http://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     */
    public function rewind()
    {
        if (!$this->init) $this->init();
        $this->position = 0;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Return the current element
     * @link http://php.net/manual/en/iterator.current.php
     * @return EntityInterface
     */
    public function current()
    {
        if (!$this->init) $this->init();
        return $this->valid() ? $this->entities[$this->position] : null;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Return the key of the current element
     * @link http://php.net/manual/en/iterator.key.php
     * @return mixed scalar on success, or null on failure.
     */
    public function key()
    {
        return $this->position;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Move forward to next element
     * @link http://php.net/manual/en/iterator.next.php
     * @return void Any returned value is ignored.
     */
    public function next()
    {
        $this->position++;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Checks if current position is valid
     * @link http://php.net/manual/en/iterator.valid.php
     * @return boolean The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     */
    public function valid()
    {
        if (!$this->init) $this->init();
        return isset($this->entities[$this->position]);
    }
}