<?php
namespace TiaraBase\Entity;

use Zend\ServiceManager\AbstractPluginManager;

/**
 * Plugin manager implementation for entities.
 *
 * @package TiaraBase\Entity
 * @method EntityInterface|EntityCollectionInterface get($name)
 */
class EntityManager extends AbstractPluginManager
{
    /** @var bool */
    protected $shareByDefault = false;

    /**
     * Validate the plugin
     *
     * Checks that the entity loaded is an instance of EntityInterface or EntityCollection.
     *
     * @param  mixed $plugin
     * @throws Exception\InvalidEntityException if the requested entity isn't a valid entity or an
     *                                          entity collection
     * @return void
     */
    public function validatePlugin($plugin)
    {
        if ($plugin instanceof EntityInterface || $plugin instanceof EntityCollectionInterface) {
            return;
        }

        throw new Exception\InvalidEntityException(sprintf(
            'Plugin of type %s is invalid; must implement TiaraBase\Entity\EntityInterface or TiaraBase\Entity\EntityCollection',
            (is_object($plugin) ? get_class($plugin) : gettype($plugin))
        ));
    }
}