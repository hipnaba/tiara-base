<?php
namespace TiaraBase\Entity\Exception;

/**
 * Exception thrown if a value does not adhere to a defined valid data domain.
 *
 * @package TiaraBase\Entity\Exception
 */
class DomainException extends \DomainException implements
    ExceptionInterface
{ }