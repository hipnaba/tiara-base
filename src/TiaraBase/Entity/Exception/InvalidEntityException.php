<?php
namespace TiaraBase\Entity\Exception;

/**
 * Exception thrown if an argument isn't a valid entity.
 *
 * @package TiaraBase\Entity\Exception
 */
class InvalidEntityException extends InvalidArgumentException
{ }