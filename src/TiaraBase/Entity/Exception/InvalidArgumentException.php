<?php
namespace TiaraBase\Entity\Exception;

/**
 * Exception thrown if an argument does not match with the expected value.
 *
 * @package TiaraBase\Entity\Exception
 */
class InvalidArgumentException extends \InvalidArgumentException implements
    ExceptionInterface
{ }