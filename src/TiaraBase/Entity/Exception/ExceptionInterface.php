<?php
namespace TiaraBase\Entity\Exception;

/**
 * Interface implemented by all exceptions in the TiaraBase\Entity module.
 *
 * @package TiaraBase\Entity\Exception
 */
interface ExceptionInterface
{ }