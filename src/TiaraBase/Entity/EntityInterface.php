<?php
namespace TiaraBase\Entity;

/**
 * Represents a persistable entity.
 *
 * Mappers can only work with entities implementing this interface. Every entity
 * has a unique ID that is used as the primary key in the database table used to
 * store entities of this type. Although implementing this interface directly
 * will work, it is recommended to extend the AbstractEntity class found in
 * this package.
 *
 * @package TiaraBase\Entity
 */
interface EntityInterface
{
    /**
     * Returns the entity ID
     *
     * @return int
     */
    public function getId();

    /**
     * Sets the entity ID
     *
     * @param int $id
     * @return $this
     */
    public function setId($id);
}