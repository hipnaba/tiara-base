<?php
namespace TiaraBase\Mapper;

use TiaraBase\Mapper\Exception\DomainException;
use TiaraBase\Mapper\Exception\InvalidMapperException;
use Zend\EventManager\EventInterface;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventsCapableInterface;
use Zend\ServiceManager\AbstractPluginManager;
use Zend\ServiceManager\ConfigInterface;
use Zend\ServiceManager\Exception;
use Zend\Stdlib\InitializableInterface;

/**
 * Plugin manager implementation for mappers.
 *
 * @package TiaraBase\Mapper
 * @method MapperInterface get($name)
 */
class MapperManager extends AbstractPluginManager implements
    EventManagerAwareInterface
{
    /** @var \Zend\EventManager\EventManagerInterface */
    protected $events;
    /** @var MapperInterface[] */
    protected $mappers = array();

    /**
     * Constructor
     *
     * @param ConfigInterface $configuration
     */
    public function __construct(ConfigInterface $configuration = null)
    {
        parent::__construct($configuration);

        $this->addInitializer(array($this, 'instantiatePrototypes'));
        $this->addInitializer(array($this, 'callMapperInit'), false);
    }

    /**
     * If the mapper has the prototypes defined as strings we try to use the EntityManager
     * to convert them to objects
     *
     * @param object $mapper
     * @throws DomainException
     */
    public function instantiatePrototypes($mapper)
    {
        if ($mapper instanceof MapperInterface) {
            /** @var \TiaraBase\Entity\EntityManager $entities */
            $entities = $this->getServiceLocator()->get('EntityManager');

            try {
                $mapper->getEntityPrototype();
            } catch (DomainException $e) {
                $entityPrototypeName = $mapper->getEntityPrototypeName();
                if (strlen($entityPrototypeName) > 0) {
                    if (!$entities->has($entityPrototypeName)) {
                        throw new DomainException(sprintf(
                            'Cannot create entity %s referenced by %s',
                            $entityPrototypeName, get_class($mapper)
                        ));
                    }

                    $entity = $entities->get($entityPrototypeName);
                    $mapper->setEntityPrototype($entity);
                    $this->mappers[$entityPrototypeName] = $mapper;
                }
            }

            try {
                $mapper->getCollectionPrototype();
            } catch (DomainException $e) {
                $collectionPrototypeName = $mapper->getCollectionPrototypeName();
                if (strlen($collectionPrototypeName) > 0) {
                    if (!$entities->has($collectionPrototypeName)) {
                        throw new DomainException(sprintf(
                            'Cannot create collection %s referenced by %s',
                            $collectionPrototypeName, get_class($mapper)
                        ));
                    }

                    $collection = $entities->get($collectionPrototypeName);
                    $mapper->setCollectionPrototype($collection);
                }
            }
        }
    }

    /**
     * Calls the init method on new mappers.
     *
     * @param object $mapper
     */
    public function callMapperInit($mapper)
    {
        if ($mapper instanceof InitializableInterface || method_exists($mapper, 'init')) {
            $mapper->init();
        }
    }

    /**
     * Inject an EventManager instance
     *
     * @param  EventManagerInterface $eventManager
     * @return $this
     */
    public function setEventManager(EventManagerInterface $eventManager)
    {
        $this->events = $eventManager;
        $this->events->getSharedManager()->attach('*', '*', array($this, 'onEvent'), 10);
        return $this;
    }

    /**
     * Retrieve the event manager
     *
     * Lazy-loads an EventManager instance if none registered.
     *
     * @return EventManagerInterface
     */
    public function getEventManager()
    {
        return $this->events;
    }

    /**
     * This method tries to figure out if the event triggered needs to be forwarded to a mapper
     *
     * @param EventInterface $event
     * @return null|\Zend\EventManager\ResponseCollection
     */
    public function onEvent(EventInterface $event)
    {
        $name = $event->getName();

        if (strpos($name, '.')) {
            list ($entity, $name) = explode('.', $name);
            $mapper = null;

            if (isset($this->mappers[$entity])) {
                $mapper = $this->mappers[$entity];
            }
            else {
                $registered = $this->getRegisteredServices();
                foreach (array('aliases', 'invokableClasses', 'factories') as $type) {
                    $services = $registered[$type];

                    foreach ($services as $service) {
                        try {
                            $try = $this->get($service);
                            if ($try->getEntityPrototypeName() == $entity) {
                                $mapper = $try;
                                $this->mappers[$entity] = $mapper;
                                break;
                            }
                        } catch (\Exception $e)
                        { }
                    }
                }
            }

            if ($mapper instanceof EventsCapableInterface) {
                $event->stopPropagation();
                $results = $mapper->getEventManager()->trigger($name, $event->getTarget(), $event->getParams());
                foreach ($results as $result) {
                    if (null !== $result) {
                        return $result;
                    }
                }
            }
        }
        return true;
    }

    /**
     * Attempt to create an instance via an invokable class.
     *
     * Overrides parent implementation by passing a db adapter to the
     * constructor.
     *
     * @param  string $canonicalName
     * @param  string $requestedName
     * @return null|MapperInterface
     * @throws \Zend\ServiceManager\Exception\ServiceNotCreatedException If resolved class does not exist
     */
    protected function createFromInvokable($canonicalName, $requestedName)
    {
        $services = $this->getServiceLocator();

        $adapter = $services->get('Zend\Db\Adapter\Adapter');
        $invokable = $this->invokableClasses[$canonicalName];
        $mapper = new $invokable($adapter);

        if ($mapper instanceof EventsCapableInterface) {
            $mapper->getEventManager();
        }

        return $mapper;
    }

    /**
     * Validate the plugin.
     *
     * Checks that the mapper loaded is an instance of MapperInterface.
     *
     * @param  mixed $plugin
     * @return void
     * @throws InvalidMapperException
     */
    public function validatePlugin($plugin)
    {
        if ($plugin instanceof MapperInterface) {
            return;
        }

        throw new InvalidMapperException(sprintf(
            'Plugin of type %s is invalid; must implement TiaraBase\Mapper\MapperInterface',
            (is_object($plugin) ? get_class($plugin) : gettype($plugin))
        ));
    }
}