<?php
namespace TiaraBase\Mapper;

/**
 * Indicates this module provides mappers.
 *
 * @package TiaraBase\Module
 */
interface MapperProviderInterface
{
    /**
     * Expected to return \Zend\ServiceManager\Config object or array to
     * seed such an object.
     *
     * @return array|\Zend\ServiceManager\Config
     */
    public function getMapperConfig();
}