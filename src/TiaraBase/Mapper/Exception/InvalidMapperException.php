<?php
namespace TiaraBase\Mapper\Exception;

/**
 * An invalid mapper encountered in the TiaraBase\Mapper namespace.
 *
 * @package TiaraBase\Mapper\Exception
 */
class InvalidMapperException extends InvalidArgumentException
{ }