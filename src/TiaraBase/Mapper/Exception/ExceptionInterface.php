<?php
namespace TiaraBase\Mapper\Exception;

/**
 * An exception thrown in the TiaraBase\Mapper namespace.
 *
 * @package TiaraBase\Mapper\Exception
 */
interface ExceptionInterface
{ }