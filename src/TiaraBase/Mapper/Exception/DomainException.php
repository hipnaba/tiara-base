<?php
namespace TiaraBase\Mapper\Exception;

use Zend\Db\Adapter\Driver\Sqlsrv\Exception\ExceptionInterface;

/**
 * A domain exception thrown in the TiaraBase\Mapper namespace
 *
 * @package TiaraBase\Mapper\Exception
 */
class DomainException extends \DomainException implements
    ExceptionInterface
{ }