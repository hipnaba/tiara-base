<?php
namespace TiaraBase\Mapper\Exception;

/**
 * An invalid argument encountered in the TiaraBase\Mapper namespace.
 *
 * @package TiaraBase\Mapper\Exception
 */
class InvalidArgumentException extends \InvalidArgumentException implements
    ExceptionInterface
{ }