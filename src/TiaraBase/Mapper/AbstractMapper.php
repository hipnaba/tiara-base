<?php
namespace TiaraBase\Mapper;

use TiaraBase\Entity\EntityCollectionInterface;
use TiaraBase\Entity\EntityInterface;
use TiaraBase\Mapper\Exception\DomainException;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Metadata\Metadata;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;
use Zend\EventManager\EventInterface;
use Zend\EventManager\EventManager;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Stdlib\Hydrator\Filter\FilterComposite;
use Zend\Stdlib\Hydrator\FilterEnabledInterface;
use Zend\Stdlib\Hydrator\HydratorInterface;

/**
 * Abstract mapper.
 *
 * @package TiaraBase\Mapper
 */
abstract class AbstractMapper implements
    MapperInterface
{
    /** @var Adapter */
    protected $adapter;
    /** @var \Zend\EventManager\EventManagerInterface */
    protected $events;
    /** @var HydratorInterface */
    protected $hydrator;
    /** @var EntityCollectionInterface */
    protected $collectionPrototype;
    /** @var string */
    protected $collectionPrototypeName = 'EntityCollection';
    /** @var EntityInterface */
    protected $entityPrototype;
    /** @var string */
    protected $entityPrototypeName;
    /** @var Sql */
    protected $sql;
    /** @var string */
    protected $table;
    /** @var Metadata */
    protected $tableMetadata;

    /**
     * Constructor
     *
     * @param Adapter $adapter
     */
    function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }

    /**
     * Tries to handle unhandled events
     *
     * @param EventInterface $event
     * @return mixed
     */
    public function onEvent(EventInterface $event)
    {
        $method = $event->getName();
        $handlerMethod = 'on' . ucfirst($method);
        $return = null;

        if (method_exists($this, $handlerMethod)) {
            $return = $this->$handlerMethod($event);
            $event->stopPropagation();
        }
        else if (method_exists($this, $method)) {
            $return = call_user_func_array(array($this, $method), $event->getParams());
            $event->stopPropagation();
        }

        $target = $event->getTarget();
        if (is_callable($target)) {
            call_user_func($target, $return);
        }
        else if ($target instanceof EntityCollectionInterface && $return instanceof EntityCollectionInterface) {
            $target->addAll($return);
        }

        return $return;
    }

    /**
     * Retrieve the event manager
     *
     * Lazy-loads an EventManager instance if none registered.
     *
     * @return \Zend\EventManager\EventManagerInterface
     */
    public function getEventManager()
    {
        if (null === $this->events) {
            $this->events = new EventManager();
            $this->events->attach('*', array($this, 'onEvent'), 20);
        }
        return $this->events;
    }

    /**
     * Returns the total number of rows in the primary table
     *
     * @return int
     */
    public function count()
    {
        return $this->countBy();
    }

    /**
     * Returns the number of rows with fields having values in the $params array
     *
     * @param array $params Keys are fields and values are values that need to be matched
     * @return int
     */
    protected function countBy($params = array())
    {
        $sql = $this->getSql();
        $select = $sql->select();
        $select->columns(array('count' => new Expression('COUNT(1)')));

        if (count($params)) {
            $where = new Where();
            foreach ($params as $field => $value) {
                $where->equalTo($field, $value);
            }
            $select->where($where);
        }

        $stmt = $sql->prepareStatementForSqlObject($select);
        $result = $stmt->execute()->current();
        return $result['count'];
    }

    /**
     * Returns all the entities
     *
     * @throws Exception\DomainException
     * @return \TiaraBase\Entity\EntityCollectionInterface
     */
    public function fetchAll()
    {
        $select = $this->getSql()->select();
        return $this->getCollection($select);
    }

    /**
     * Returns an entity with the specified id or null if it doesn't exist
     *
     * @param int $id
     * @return null|\TiaraBase\Entity\EntityInterface
     */
    public function findById($id)
    {
        return $this->findOneBy('id', $id);
    }

    /**
     * Returns one entity where $field equals to $value
     *
     * @param string $field
     * @param mixed $value
     * @return EntityInterface|null
     */
    protected function findOneBy($field, $value)
    {
        return $this->findBy($field, $value)->current();
    }

    /**
     * Returns a collection containing entities where $field equals to $value
     *
     * @param string $field
     * @param mixed $value
     * @return EntityCollectionInterface
     */
    protected function findBy($field, $value)
    {
        $where = new Where();
        $where->equalTo($field, $value);

        $select = $this->getSql()->select();
        $select->where($where);

        return $this->getCollection($select);
    }

    /**
     * Returns the rows from a table
     *
     * @param Select $select
     * @return HydratingResultSet
     */
    protected function select(Select $select)
    {
        $sql = $this->getSql();
        $stmt = $sql->prepareStatementForSqlObject($select);
        $resultSet = new HydratingResultSet($this->getHydrator(), $this->getEntityPrototype());

        $resultSet->initialize($stmt->execute());
        return $resultSet;
    }

    /**
     * Persists the entity
     *
     * @param EntityInterface $entity
     * @return bool
     */
    public function save(EntityInterface $entity)
    {
        $data = $this->entityToArray($entity);

        if (null === $entity->getId()) {
            $result = $this->insert($data);
            $id = $result->getGeneratedValue();
            $entity->setId($id);
        }
        else {
            $where = new Where();
            $where->equalTo('id', $entity->getId());
            $this->update($data, $where);
        }
        return true;
    }

    /**
     * Inserts a new row in the table
     *
     * @param array $data
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */
    protected function insert(array $data)
    {
        $sql = $this->getSql();
        $insert = $sql->insert();

        $insert->values($data);

        $stmt = $sql->prepareStatementForSqlObject($insert);
        return $stmt->execute();
    }

    /**
     * Updates an existing row in the table
     *
     * @param array $data
     * @param $where
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */
    protected function update(array $data, $where)
    {
        $sql = $this->getSql();
        $update = $sql->update();

        $update->set($data)->where($where);

        $stmt = $sql->prepareStatementForSqlObject($update);
        return $stmt->execute();
    }

    /**
     * Deletes the entity from the database
     *
     * @param EntityInterface $entity
     * @return bool
     */
    public function delete(EntityInterface $entity)
    {
        $where = new Where();
        $where->equalTo('id', $entity->getId());

        $sql = $this->getSql();
        $delete = $sql->delete();
        $delete->where($where);

        $stmt = $sql->prepareStatementForSqlObject($delete);
        $stmt->execute();
        return true;
    }

    /**
     * Maps the entity to an array
     *
     * @param EntityInterface $entity
     * @return array
     */
    protected function entityToArray(EntityInterface $entity)
    {
        $hydrator = $this->getHydrator();
        $data = $hydrator->extract($entity);

        return $data;
    }

    /**
     * Returns a collection that will populate itself using the provided Select
     *
     * @param Select $select
     * @return \TiaraBase\Entity\EntityCollectionInterface
     */
    protected function getCollection(Select $select)
    {
        $self = $this;
        $collection = $this->getCollectionPrototype();

        $collection->setInitializer(function (EntityCollectionInterface $collection) use ($self, $select) {
            $resultSet = $self->select($select);
            foreach ($resultSet as $entity) {
                $collection->add($entity);
            }
        });

        return $collection;
    }

    /**
     * Returns the sql object bound to the primary table
     *
     * @return Sql
     */
    protected function getSql()
    {
        if (null === $this->sql) {
            $this->sql = new Sql($this->adapter, $this->table);
        }
        return $this->sql;
    }

    /**
     * Returns the table metadata
     *
     * @return Metadata
     */
    protected function getTableMetadata()
    {
        if (null === $this ->tableMetadata) {
            $this->tableMetadata = new Metadata($this->adapter);
        }
        return $this->tableMetadata;
    }

    /**
     * Returns the entity this mapper maps to
     *
     * @throws Exception\DomainException
     * @return EntityInterface
     */
    public function getEntityPrototype()
    {
        if (!$this->entityPrototype instanceof EntityInterface) {
            throw new DomainException(sprintf(
                'Entity prototype of type %s is invalid; must implement TiaraBase\Entity\EntityInterface',
                (is_object($this->entityPrototype) ? get_class($this->entityPrototype) : gettype($this->entityPrototype))
            ));
        }
        return clone $this->entityPrototype;
    }

    /**
     * Sets the entity this mapper maps to
     *
     * @param EntityInterface $entityPrototype
     * @return $this
     */
    public function setEntityPrototype(EntityInterface $entityPrototype)
    {
        $this->entityPrototype = $entityPrototype;
        return $this;
    }

    /**
     * Returns the name of the entity this mapper maps to
     *
     * @return string
     */
    public function getEntityPrototypeName()
    {
        return $this->entityPrototypeName;
    }

    /**
     * Sets the name of the entity this mapper maps to
     *
     * @param string $entityPrototypeName
     * @return $this
     */
    public function setEntityPrototypeName($entityPrototypeName)
    {
        $this->entityPrototypeName = $entityPrototypeName;
    }

    /**
     * Returns the collections class
     *
     * @throws Exception\DomainException
     * @return EntityCollectionInterface
     */
    public function getCollectionPrototype()
    {
        if (!$this->collectionPrototype instanceof EntityCollectionInterface) {
            throw new DomainException(sprintf(
                'Collection prototype of type %s is invalid; must implement TiaraBase\Entity\EntityCollectionInterface',
                (is_object($this->collectionPrototype) ? get_class($this->collectionPrototype) : gettype($this->collectionPrototype))
            ));
        }
        return clone $this->collectionPrototype;
    }

    /**
     * Sets the collection class
     *
     * @param EntityCollectionInterface $collectionPrototype
     * @return $this
     */
    public function setCollectionPrototype(EntityCollectionInterface $collectionPrototype)
    {
        $this->collectionPrototype = $collectionPrototype;
        return $this;
    }

    /**
     * Returns the name of the collection this mapper uses for returning multiple entities
     *
     * @return mixed
     */
    public function getCollectionPrototypeName()
    {
        return $this->collectionPrototypeName;
    }

    /**
     * Sets the name of the collection this mapper uses for returning multiple entities
     *
     * @param string $collectionPrototypeName
     * @return $this
     */
    public function setCollectionPrototypeName($collectionPrototypeName)
    {
        $this->collectionPrototypeName = $collectionPrototypeName;
    }

    /**
     * Returns the hydrator this mapper uses
     *
     * @return HydratorInterface
     */
    public function getHydrator()
    {
        if (null === $this->hydrator) {
            $this->setHydrator(new ClassMethods(false));
        }
        return $this->hydrator;
    }

    /**
     * Sets the hydrator for this mapper to use
     *
     * @param HydratorInterface $hydrator
     * @return $this
     */
    public function setHydrator(HydratorInterface $hydrator)
    {
        if ($hydrator instanceof FilterEnabledInterface) {
            if (!$hydrator->hasFilter('columns')) {
                $columns = $this->getTableMetadata()->getColumnNames($this->table);
                $hydrator->addFilter('columns', function ($property) use ($columns) {
                    list(, $method) = explode('::', $property);
                    $column = lcfirst(substr($method, 3));
                    return in_array($column, $columns);
                }, FilterComposite::CONDITION_AND);
            }
        }
        $this->hydrator = $hydrator;
        return $this;
    }
}