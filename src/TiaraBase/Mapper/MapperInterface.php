<?php
namespace TiaraBase\Mapper;

use Countable;
use TiaraBase\Entity\EntityCollectionInterface;
use TiaraBase\Entity\EntityInterface;
use Zend\EventManager\EventsCapableInterface;
use Zend\Stdlib\Hydrator\HydratorAwareInterface;

/**
 * A mapper.
 *
 * @package TiaraBase\Mapper
 */
interface MapperInterface extends
    Countable,
    EventsCapableInterface,
    HydratorAwareInterface
{
    /**
     * Returns all the entities
     *
     * @return \TiaraBase\Entity\EntityCollectionInterface
     */
    public function fetchAll();

    /**
     * Returns an entity with the specified id or null if it doesn't exist
     *
     * @param int $id
     * @return null|\TiaraBase\Entity\EntityInterface
     */
    public function findById($id);

    /**
     * Persists the entity to the database
     *
     * @param EntityInterface $entity
     * @return bool
     */
    public function save(EntityInterface $entity);

    /**
     * Deletes the entity from the database
     *
     * @param EntityInterface $entity
     * @return bool
     */
    public function delete(EntityInterface $entity);

    /**
     * Returns the entity this mapper maps to
     *
     * @return EntityInterface
     */
    public function getEntityPrototype();

    /**
     * Sets the entity this mapper maps to
     *
     * @param EntityInterface $entityPrototype
     * @return $this
     */
    public function setEntityPrototype(EntityInterface $entityPrototype);

    /**
     * Returns the name of the entity this mapper maps to
     *
     * @return string
     */
    public function getEntityPrototypeName();

    /**
     * Sets the name of the entity this mapper maps to
     *
     * @param string $entityPrototypeName
     * @return $this
     */
    public function setEntityPrototypeName($entityPrototypeName);

    /**
     * Returns the collection this mapper uses for returning multiple entities
     *
     * @return EntityCollectionInterface
     */
    public function getCollectionPrototype();

    /**
     * Sets the collection this mapper uses for returning multiple entities
     *
     * @param EntityCollectionInterface $collectionPrototype
     * @return $this
     */
    public function setCollectionPrototype(EntityCollectionInterface $collectionPrototype);

    /**
     * Returns the name of the collection this mapper uses for returning multiple entities
     *
     * @return mixed
     */
    public function getCollectionPrototypeName();

    /**
     * Sets the name of the collection this mapper uses for returning multiple entities
     *
     * @param string $collectionPrototypeName
     * @return $this
     */
    public function setCollectionPrototypeName($collectionPrototypeName);
}