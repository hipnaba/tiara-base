<?php
namespace TiaraBase\Mapper;

use Zend\Mvc\Service\AbstractPluginManagerFactory;

/**
 * Creates a new mapper manager.
 *
 * @package TiaraBase\Service
 */
class MapperManagerFactory extends AbstractPluginManagerFactory
{
    const PLUGIN_MANAGER_CLASS = 'TiaraBase\Mapper\MapperManager';
}