<?php
namespace TiaraBase;

use TiaraBase\Entity\EntityProviderInterface;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ControllerPluginProviderInterface;
use Zend\ModuleManager\Feature\InitProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\ModuleManager\ModuleManagerInterface;

/**
 * This module provides the base for all Tiara modules.
 *
 * @package TiaraBase
 */
class Module implements
    AutoloaderProviderInterface,
    ConfigProviderInterface,
    ControllerPluginProviderInterface,
    EntityProviderInterface,
    InitProviderInterface,
    ServiceProviderInterface
{
    /**
     * Initialize workflow.
     *
     * @param  ModuleManagerInterface $manager
     * @return void
     */
    public function init(ModuleManagerInterface $manager)
    {
        /** @var \Zend\ModuleManager\ModuleManager $manager */
        /** @var \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $manager->getEvent()->getParam('ServiceManager');
        /** @var \Zend\ModuleManager\Listener\ServiceListenerInterface $serviceListener */
        $serviceListener = $serviceLocator->get('ServiceListener');

        $serviceListener->addServiceManager(
            'EntityManager',
            'entities',
            'TiaraBase\Entity\EntityProviderInterface',
            'getEntityConfig'
        );

        $serviceListener->addServiceManager(
            'MapperManager',
            'mappers',
            'TiaraBase\Mapper\MapperProviderInterface',
            'getMapperConfig'
        );
    }

    /**
     * Return an array for passing to Zend\Loader\AutoloaderFactory.
     *
     * @return array
     */
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php'
            ),
        );
    }

    /**
     * Returns configuration to merge with application configuration
     *
     * @return array|\Traversable
     */
    public function getConfig()
    {
        return array(
            'translator' => array(
                'translation_file_patterns' => array(
                    array(
                        'type'     => 'gettext',
                        'base_dir' => __DIR__ . '/language',
                        'pattern'  => '%s.mo',
                    ),
                ),
            ),
        );
    }

    /**
     * Expected to return \Zend\ServiceManager\Config object or array to
     * seed such an object.
     *
     * @return array|\Zend\ServiceManager\Config
     */
    public function getControllerPluginConfig()
    {
        return array(
            'aliases' => array(
                'Wrapper' => 'TiaraBase\Controller\Plugin\Wrapper'
            ),
            'invokables' => array(
                'TiaraBase\Controller\Plugin\Wrapper' => 'TiaraBase\Controller\Plugin\Wrapper'
            )
        );
    }

    /**
     * Expected to return \Zend\ServiceManager\Config object or array to
     * seed such an object.
     *
     * @return array|\Zend\ServiceManager\Config
     */
    public function getEntityConfig()
    {
        return array(
            'aliases' => array(
                'EntityCollection' => 'TiaraBase\Entity\EntityCollectionInterface'
            ),
            'invokables' => array(
                'TiaraBase\Entity\EntityCollectionInterface' => 'TiaraBase\Entity\EntityCollection'
            )
        );
    }

    /**
     * Expected to return \Zend\ServiceManager\Config object or array to
     * seed such an object.
     *
     * @return array|\Zend\ServiceManager\Config
     */
    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'EntityManager' => 'TiaraBase\Entity\EntityManagerFactory',
                'MapperManager' => 'TiaraBase\Mapper\MapperManagerFactory'
            )
        );
    }
}