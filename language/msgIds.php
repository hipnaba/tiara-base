<?php
////////////////////
// Zend\Validator //
////////////////////

/** Identical */
_('The two given tokens do not match');
_('No token was provided to match against');
/** NotEmpty */
_('Value is required and can\'t be empty');
_('Invalid type given. String, integer, float, boolean or array expected');