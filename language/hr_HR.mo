��          D      l       �   E   �   &   �   !   �   $     �  =  U   2  %   �     �  -   �                          Invalid type given. String, integer, float, boolean or array expected No token was provided to match against The two given tokens do not match Value is required and can't be empty Project-Id-Version: tiara-base
POT-Creation-Date: 2014-09-08 15:35+0100
PO-Revision-Date: 2014-09-08 15:36+0100
Last-Translator: Danijel Fabijan <hipnaba@gmail.com>
Language-Team: hipnaba <hipnaba@gmail.com>
Language: hr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
X-Poedit-KeywordsList: _;gettext;gettext_noop
X-Poedit-Basepath: .
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: /home/hipnaba/projects/tiara-base/language
 Neispravna vrijednost. Vrijednost mora biti string, integer, float, boolean ili array Vrijednost za usporedbu nije navedena Vrijednosti se ne podudaraju Vrijednost je obavezna i ne smije biti prazna 